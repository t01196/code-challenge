#Desafio back-end
##Sobre o desafio

Dependendo da sua experiência e/ou das ferramentas escolhidas, você pode precisar de mais ou de menos tempo para realizar o desafio.

Para isso, vamos fornecer para você alguns dados. Neste json você encontra ...lorem ipsum:

{
  
}


Usando as informações anteriores, crie uma API REST que execute as funções abaixo. Você pode fazer tudo em memória, ou seja, não precisa utilizar nenhum banco de dados ou ferramenta. Se você preferir, se sentir mais confortável ou achar mais fácil, fique a vontade! ;)
1. Lorem ipsum :)


Request:

POST /properties

Body:

{

}

Response:

Você define, faz parte da avaliação.
2. Lorem ipsum =]



Request:

  GET 

Response:

{
  
}

3. Lorem ipsum :D



Request:

  GET 

Response:

{
 
}

4. Wow! Agora temos que fazer deploy! :D

Crie uma documentação de como rodar o seu projeto! Quanto mais simples, melhor! =D
Modo de avaliação

Nós sempre avaliamos o seu código, e para isso nós envolvemos sempre no mínimo 3 engenheiros aqui da Digital e amigavelmente informamos que iremos nos basear pelos seguintes critérios:

    Manutenibilidade: O código é legível e de fácil manutenção?
    Desenho: Como foram separadas as responsabilidades? Quais técnicas foram utilizadas?
    Qualidade: Tem testes? Quão difícil é recriar os testes caso seja necessário alterar o comportamento da aplicação?
    Desempenho: Escreveu um código com performance adequada? Não precisa ser perfeito, mas entende como seria a melhor solução?

Fique a vontade para incrementar seu desafio de modo a demonstrar como o resultado do seu esforço pode deixá-lo ainda melhor!

Bom código! ;)